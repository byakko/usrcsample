app.Model = app.Model or {}

app.Model.SubSystemCollection = class
	constructor: ->
		@help = new app.Shared.CollectionHelp @
		@help.init()

		@$formBench = $('#formBench')

		@subSysBench = ko.observable()
		@benchType = ko.observable()

		@pagination = new app.Shared.Pagination @

		@add = =>
			subSys = @help.new {}
			@subSysBench subSys
			do subSys.activeValidate
			@benchType 'create'

		@update = (subSys) =>
			subSys.help.fetch().done (data) =>
				subSys = @help.new data
				@subSysBench subSys
				do subSys.activeValidate
				@benchType 'update'
			.fail (data) =>
				do app.Shared.Common.alertError
				@$formBench.modal('hide')

		@submit = (subSys) =>
			switch @benchType()
				when 'create'
					successMessage = '用户已添加'
				when 'update'
					successMessage = '修改成功'
			if !subSys.isValid()
				subSys.errors.showAllMessages(true)
				return
			subSys.help.saveChanges().done (data, status) =>
				app.Shared.Common.alert successMessage, 'success'
				@pagination.reload()
				@$formBench.modal('hide')
			.fail (data, status) =>
				app.Shared.Common.alert(data.responseJSON.Message, 'error')
				@help.messageRender data, subSys

		@delete = (subSys) =>
			app.Common.confirm.message('确认删除?').done =>
				subSys.help.delete().done (data, status) =>
					app.Shared.Common.alert '删除成功', 'success' if status == 'success'
					@pagination.reload()
				.fail (data, status) =>
					app.Shared.Common.alert data.responseJSON.Message, 'error'

	model: app.Model.SubSystem
	dataRoot: 'Root'
	progressMessage: '数据读取中...'
