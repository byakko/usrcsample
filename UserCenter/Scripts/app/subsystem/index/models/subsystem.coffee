app.Model = app.Model or {}

app.Model.SubSystem = class
	constructor: (data) ->
		@help = new app.Shared.ModelHelp @
		@help.init(data)
		@errors = ko.validation.group(@)

	key: 'PKID'

	url: '/api/SubSystems'

	persist: [
		'PKID'
		'Code'
		'SysName'
		'AppKey'
		'Path'
		'Scale'
	]

	activeValidate: ->
		@SysName.extend
			required:
				message: '系统名不能为空'
		@Code.extend
			required:
				message: '编号不能为空'
		@AppKey.extend
			required:
				message: 'AppKey不能为空'
		@Path.extend
			required:
				message: 'Path不能为空'
		@Scale.extend
			required:
				message: '比例不能为空'
			number:
				message: '必须为数字'