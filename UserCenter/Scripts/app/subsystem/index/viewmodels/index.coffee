app.ViewModel = app.ViewModel or {}

app.ViewModel.Index = class
	constructor: ->
		@subSystemCollection = new app.Model.SubSystemCollection
		@confirm = app.Common.confirm
		@subSystemCollection.help.load
			page: 1
