app = app || {}
app.Shared = app.Shared or {}

app.Shared.Pagination = class
	constructor: (@collection) ->
		@totalPage = ko.computed =>
			Math.ceil(@collection.totalLength() / 10)
		@currentPage = ko.observable(1)
		@pages = ko.observableArray []

		@totalPage.subscribe @pageInit, @

	changePage: (page) ->
		index = if page.index == 0 then (if page.arrowLeft and @currentPage() > 1 then @currentPage() - 1 else if page.arrowRight and @currentPage() < @totalPage() then @currentPage() + 1 else 0) else page.index
		return if index  == 0
		@currentPage index
		do @reload
		do @pageInit

	reload: () ->
		do @collection.help.load

	pageInit: ->
		currentPage = @currentPage()
		totalPage = if @totalPage() then @totalPage() else 1
		newPages = [{
			content: currentPage
			index: currentPage
		}]
		padding =
			content: '...'
			index: 0
		headPage =
			content: 1
			index: 1
		tailPage =
			content: totalPage
			index: totalPage
		prePage =
			content: ''
			index: 0
			arrowLeft: true
		nextPage =
			content: ''
			index: 0
			arrowRight: true
		head = 1
		tail = totalPage
		count = 5

		forward = 'tail'

		cursors =
			head:
				index: currentPage
				step: -1
				reached: false
			tail:
				index: currentPage
				step: 1
				reached: false

		add = ->
			return if newPages.length >= count
			newIndex = cursors[forward].index + cursors[forward].step
			if newIndex > tail or newIndex < head
				cursors[forward].reached = true
				return if cursors['head'].reached and cursors['tail'].reached
			else
				location = if forward == 'head' then 0 else newPages.length
				newPages.splice(location, 0, {
					content: newIndex
					index: newIndex
				})
				cursors[forward].index = newIndex
			forward = if forward == 'head' then 'tail' else 'head'
			add()

		add()
		newPages.push padding unless tail <= cursors.tail.index + 1
		newPages.unshift padding unless head >= cursors.head.index - 1
		newPages.push tailPage unless tail <= cursors.tail.index
		newPages.unshift headPage unless head >= cursors.head.index
		newPages.push nextPage
		newPages.unshift prePage
		@pages newPages
