// Generated by CoffeeScript 1.6.3
var app;

app = app || {};

app.Shared = app.Shared || {};

app.Shared.Filter = (function() {
  function _Class(collection, options) {
    var _this = this;
    this.collection = collection;
    if (options == null) {
      options = {};
    }
    this.filterList = [];
    _.each(options, function(value, prop, opt) {
      _this[prop] = ko.observable().extend({
        throttle: 500
      });
      _this[prop].subscribe(function() {
        if (_this.collection.pagination) {
          return _this.collection.pagination.changePage({
            index: 1
          });
        } else {
          return _this.collection.help.load();
        }
      });
      return _this.filterList.push({
        property: prop,
        observable: _this[prop]
      });
    });
  }

  _Class.prototype.additionParams = function() {
    return _.reduce(this.filterList, function(filter, item) {
      if (item.observable()) {
        filter[item.property] = item.observable();
      }
      return filter;
    }, {});
  };

  return _Class;

})();
