app = app or {}
app.Shared = app.Shared or {}

app.Shared.ModelHelp = class
	constructor: (@model) ->

	isHelper: true

	init: (data) ->
		@model['_key'] = data[@model['key']]
		ko.utils.arrayForEach @model.persist, (attr) =>
			@model[attr] = ko.observable(data[attr])
		@adapter = if @model.local then app.Shared.Adapter.Local else app.Shared.Adapter.Remote

	fetch: (options = {}) ->
		@adapter.fetch @, options

	saveChanges: (options = {}) ->
		persistObject = {}
		ko.utils.objectForEach @model, (name, value) =>
			if ko.utils.arrayIndexOf(@model.persist, name) != -1
				persistObject[name] = value

		@adapter.saveChanges @, $.extend
			data: ko.toJS(persistObject)
			, options

	delete: (options = {}) ->
		@adapter.delete @, options

	requestUrl: ->
		@adapter.requestUrl(@)