app = app || {}
app.Shared = app.Shared or {}

app.Shared.Filter = class
	constructor: (@collection, options = {}) ->
		@filterList = []
		_.each options, (value, prop, opt) =>
			@[prop] = ko.observable().extend({ throttle: 500 })
			@[prop].subscribe =>
				if (@collection.pagination)
					# Turn to page 1
					@collection.pagination.changePage
						index: 1
				else
					do @collection.help.load
			@filterList.push
				property: prop
				observable: @[prop]

	additionParams: ->
		_.reduce @filterList, (filter, item) ->
			filter[item.property] = item.observable() if item.observable()
			filter
		, {}