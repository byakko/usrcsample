app = app or {}
app.Shared = app.Shared or {}

app.Shared.CollectionHelp = class
	constructor: (@collecion) ->

	isHelper: true

	init: (data = []) ->
		@collecion.totalLength = ko.observable()
		@collecion.data = ko.observableArray []
		@reset data
		@adapter = if @collecion.model.prototype.local then app.Shared.Adapter.Local else app.Shared.Adapter.Remote

	load: (params) ->
		params = $.extend page: @collecion.pagination.currentPage(), params if @collecion.pagination
		params = $.extend @collecion.filter.additionParams(), params if @collecion.filter

		if !@collecion.model.prototype.local and @collecion.progressMessage
			config = @adapter.getConfig(@)
			delayObj = Messenger().run
				progressMessage: @collecion.progressMessage
			,
				url: config.url
				data: params
		else
			delayObj = @adapter.fetch(@, data: params)

		delayObj.done (allData) =>
			console.log allData
			[data, totalLength] = if @collecion.dataRoot then [allData[@collecion.dataRoot], allData.TotalLength] else [allData, allData.length]
			@reset data
			@collecion.totalLength totalLength

	new: (data) ->
		new @collecion.model(data)

	reset: (data) ->
		mappedModels = $.map data, (item) => new @collecion.model(item)
		@collecion.data mappedModels

	messageRender: (data, instance) ->
		ko.utils.objectForEach data.responseJSON.ModelState, (prop, list) =>
			field = prop.split('.').pop()
			if instance[field]
				instance[field].error = list[0]
				instance[field].__valid__(false)
		instance.errors.showAllMessages(true)
