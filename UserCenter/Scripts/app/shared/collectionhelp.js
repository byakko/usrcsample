// Generated by CoffeeScript 1.6.3
var app;

app = app || {};

app.Shared = app.Shared || {};

app.Shared.CollectionHelp = (function() {
  function _Class(collecion) {
    this.collecion = collecion;
  }

  _Class.prototype.isHelper = true;

  _Class.prototype.init = function(data) {
    if (data == null) {
      data = [];
    }
    this.collecion.totalLength = ko.observable();
    this.collecion.data = ko.observableArray([]);
    this.reset(data);
    return this.adapter = this.collecion.model.prototype.local ? app.Shared.Adapter.Local : app.Shared.Adapter.Remote;
  };

  _Class.prototype.load = function(params) {
    var config, delayObj,
      _this = this;
    if (this.collecion.pagination) {
      params = $.extend({
        page: this.collecion.pagination.currentPage()
      }, params);
    }
    if (this.collecion.filter) {
      params = $.extend(this.collecion.filter.additionParams(), params);
    }
    if (!this.collecion.model.prototype.local && this.collecion.progressMessage) {
      config = this.adapter.getConfig(this);
      delayObj = Messenger().run({
        progressMessage: this.collecion.progressMessage
      }, {
        url: config.url,
        data: params
      });
    } else {
      delayObj = this.adapter.fetch(this, {
        data: params
      });
    }
    return delayObj.done(function(allData) {
      var data, totalLength, _ref;
      console.log(allData);
      _ref = _this.collecion.dataRoot ? [allData[_this.collecion.dataRoot], allData.TotalLength] : [allData, allData.length], data = _ref[0], totalLength = _ref[1];
      _this.reset(data);
      return _this.collecion.totalLength(totalLength);
    });
  };

  _Class.prototype["new"] = function(data) {
    return new this.collecion.model(data);
  };

  _Class.prototype.reset = function(data) {
    var mappedModels,
      _this = this;
    mappedModels = $.map(data, function(item) {
      return new _this.collecion.model(item);
    });
    return this.collecion.data(mappedModels);
  };

  _Class.prototype.messageRender = function(data, instance) {
    var _this = this;
    ko.utils.objectForEach(data.responseJSON.ModelState, function(prop, list) {
      var field;
      field = prop.split('.').pop();
      if (instance[field]) {
        instance[field].error = list[0];
        return instance[field].__valid__(false);
      }
    });
    return instance.errors.showAllMessages(true);
  };

  return _Class;

})();
