app = app or {}
app.Shared = app.Shared or {}

app.Shared.Confirm = class
	constructor: ->
		@title = ko.observable '提示'
		@content = ko.observable()

	message: (msg) ->
		@content msg
		@cache = do $.Deferred
		@cache

	confirm: ->
		do @cache.resolve

	cancel: ->
		do @cache.reject
