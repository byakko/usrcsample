app = app or {}
app.Shared = app.Shared or {}
app.Shared.Adapter = app.Shared.Adapter or {}

app.Shared.Adapter.Remote = class
	@requestUrl: (helper) ->
		@getConfig(helper).url

	@fetch: (config, options) ->
		config = @getConfig(config, 'get') if config.isHelper
		options = $.extend true, options, config.extra if config.extra
		$.ajax $.extend
			url: config.url
			type: 'GET'
			dataType: 'json'
			,
			options

	@saveChanges: (config, options) ->
		config = @getConfig(config, 'change') if config.isHelper
		console.log JSON.stringify options.data
		if config.single
			$.ajax $.extend
				url: config.url
				type: if config.hasKey then 'PUT' else 'POST'
				,
				options

	@delete: (config, options) ->
		config = @getConfig(config) if config.isHelper
		if config.single
			$.ajax $.extend
				url: config.url
				type: 'DELETE'
				,
				options

	# WcfRest config
	# @getConfig: (helper, type) ->
	# 	config = {}
	# 	model = if helper.model then helper.model else helper.collecion.model.prototype
	# 	baseUrl = if model.host then 'http://' + model.host + '/' else '/'
	# 	switch type
	# 		when 'get'
	# 			config.extra =
	# 				data:
	# 					apikey: model.apiKey
	# 			if helper.model
	# 				config.url = baseUrl + 'get' + model.entity
	# 				config.single = true
	# 				$.extend true, config.extra,
	# 					data: _.object [
	# 						model.key
	# 					], [
	# 						model['_key']
	# 					]
	# 			else
	# 				config.url = baseUrl + 'get' + model.entity + 's'
	# 		when 'change'
	# 			config.single = true
	# 			if model['_key']
	# 				config.url = baseUrl + 'update' + model.entity + '/?apikey=' + model.apiKey
	# 				config.hasKey = true
	# 			else
	# 				config.url = baseUrl + 'add' + model.entity + '/?apikey=' + model.apiKey
	# 				config.hasKey = false
	# 	config

	# Original config
	@getConfig: (helper) ->
		if helper.model
			if helper.model['_key']
				url: helper.model.url + '/' + helper.model['_key']
				single: true
				hasKey: true
			else
				url: helper.model.url
				single: true
				hasKey: false
		else
			url: helper.collecion.model.prototype.url
			single: false

app.Shared.Adapter.Local = class
	@DBName: 'UserCenter'

	@getStore: (storeName) ->
		$.indexedDB(@DBName)
		.objectStore(storeName, true)

	@fetch: (config, options = {}) ->
		config = @getConfig(config) if config.isHelper
		if config.single
			$.Deferred (deferred) =>
				@getStore(config.objectStore).get(config.keyVal)
				.done (value) ->
					deferred.resolve($.extend((_.object [
						config.keyName
					], [
						config.keyVal
					]), value), 'success')
		else
			$.Deferred (deferred) =>
				resources = []
				@getStore(config.objectStore)
				.each (instance) ->
					resources.push $.extend((_.object [
						config.keyName
					], [
						instance.key
					]), instance.value)
					undefined
				.done =>
					response = @localFilter resources, options.data, config
					deferred.resolve(response, 'success')

	@localFilter: (resources, rules = {}, config) ->
		count = resources.length
		chain = _.chain(resources)
		_.each rules, (value, key, obj) ->
			switch key
				when 'page'
					limit = obj.limit or 10
					chain = _.chain(chain.value().slice((value - 1) * limit, value * limit))
		resources = chain.value()
		if rules.page then _.object [
			config.dataRoot
			'totalLength'
		], [
			resources
			count
		] else resources

	@saveChanges: (config, options) ->
		config = @getConfig(config) if config.isHelper
		if config.single
			if config.keyVal
				@getStore(config.objectStore).put options.data, config.keyVal
			else
				@getStore(config.objectStore).add options.data

	@delete: (config, options) ->
		config = @getConfig(config) if config.isHelper
		if config.single
			@getStore(config.objectStore).delete config.keyVal

	@getConfig: (helper) ->
		if helper.model
			objectStore: helper.model.objectStore
			single: true
			keyName: helper.model.key
			keyVal: helper.model['_key']
		else
			objectStore: helper.collecion.model.prototype.objectStore
			single: false
			keyName: helper.collecion.model.prototype.key
			dataRoot: helper.collecion.dataRoot