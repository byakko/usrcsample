// Generated by CoffeeScript 1.6.3
app.Model = app.Model || {};

app.Model.UserCollection = (function() {
  function _Class() {
    var _this = this;
    this.help = new app.Shared.CollectionHelp(this);
    this.help.init();
    this.$formBench = $('#formBench');
    this.$pointBench = $('#pointBench');
    this.userBench = ko.observable();
    this.benchType = ko.observable();
    this.pointBench = ko.observable();
    this.sourceType = ko.observable('cloud');
    this.extraButtenVisable = ko.computed(function() {
      return _this.sourceType() === 'cloud';
    });
    this.pagination = new app.Shared.Pagination(this);
    this.filter = new app.Shared.Filter(this, {
      Name: true
    });
  }

  _Class.prototype.model = app.Model.User;

  _Class.prototype.dataRoot = 'Root';

  _Class.prototype.sourceList = [
    {
      name: '专家平台',
      type: 'cloud',
      url: '/api/CldUsrs',
      objectStore: 'cldUsers'
    }, {
      name: '云客户端',
      type: 'bhyf',
      url: '/api/BHYFUsrs',
      objectStore: 'bhyfUsers'
    }
  ];

  _Class.prototype.add = function() {
    var user;
    user = this.help["new"]({});
    if (user.local) {
      user.IsActive(false);
      user.Creator('CurrentUser');
      user.Createtime(new Date);
    }
    this.userBench(user);
    user.activeValidate();
    this.$formBench.find('[data-toggle="checkbox"]').checkbox();
    return this.benchType('create');
  };

  _Class.prototype.update = function(user) {
    var _this = this;
    return user.help.fetch().done(function(data) {
      user = _this.help["new"](data);
      _this.userBench(user);
      user.activeValidate();
      _this.$formBench.find('[data-toggle="checkbox"]').checkbox();
      return _this.benchType('update');
    }).fail(function(data) {
      return app.Shared.Common.alertError();
    });
  };

  _Class.prototype.submit = function(user) {
    var successMessage;
    switch (this.benchType()) {
      case 'create':
        successMessage = '用户已添加';
        break;
      case 'update':
        successMessage = '修改成功';
    }
    return this._userSave(user, successMessage, this.$formBench);
  };

  _Class.prototype.submitPoint = function(user) {
    return this._userSave(user, '积分保存成功', this.$pointBench);
  };

  _Class.prototype._userSave = function(user, successMessage, $form) {
    var _this = this;
    if (!user.isValid()) {
      user.errors.showAllMessages(true);
      return;
    }
    return user.help.saveChanges().done(function(data, status) {
      app.Shared.Common.alert(successMessage, 'success');
      _this.pagination.reload();
      return $form.modal('hide');
    }).fail(function(data, status) {
      app.Shared.Common.alert(data.responseJSON.Message, 'error');
      return _this.help.messageRender(data, user);
    });
  };

  _Class.prototype.modifyPoint = function(user) {
    var _this = this;
    return user.help.fetch().done(function(data) {
      user = _this.help["new"](data);
      user.activeValidate();
      return _this.pointBench(user);
    }).fail(function(data) {
      return app.Common.alertError();
    });
  };

  _Class.prototype.changeSource = function(source) {
    this.sourceType(source.type);
    app.Model.User.prototype.url = source.url;
    app.Model.User.prototype.objectStore = source.objectStore;
    return this.pagination.changePage({
      index: 1
    });
  };

  _Class.prototype.resetPwd = function(user) {
    return app.Common.confirm.message('确认重置密码?').done(function() {
      var _this = this;
      return user.resetPwd().done(function(data, status) {
        if (status === 'success') {
          return app.Shared.Common.alert('密码已重置', 'success');
        }
      });
    });
  };

  _Class.prototype["delete"] = function(user) {
    return app.Common.confirm.message('确认删除?').done(function() {
      var _this = this;
      return user.help["delete"]().done(function(data, status) {
        if (status === 'success') {
          app.Shared.Common.alert('删除成功', 'success');
        }
        return _this.pagination.reload();
      });
    });
  };

  return _Class;

})();
