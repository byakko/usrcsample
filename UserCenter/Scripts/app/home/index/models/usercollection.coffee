app.Model = app.Model or {}

app.Model.UserCollection = class
	constructor: ->
		@help = new app.Shared.CollectionHelp @
		@help.init()

		@$formBench = $('#formBench')
		@$pointBench = $('#pointBench')

		@userBench = ko.observable()
		@benchType = ko.observable()

		@pointBench = ko.observable()

		@sourceType = ko.observable('cloud')
		@extraButtenVisable = ko.computed => @sourceType() == 'cloud'

		@pagination = new app.Shared.Pagination @
		@filter = new app.Shared.Filter @,
			Name: true

	model: app.Model.User
	dataRoot: 'Root'
	sourceList: [
		{
			name: '专家平台'
			type: 'cloud'
			url: '/api/CldUsrs'
			objectStore: 'cldUsers'
		}
		{
			name: '云客户端'
			type: 'bhyf'
			url: '/api/BHYFUsrs'
			objectStore: 'bhyfUsers'
		}
	]

	add: ->
		user = @help.new {}
		if user.local
			user.IsActive(false)
			user.Creator('CurrentUser')
			user.Createtime(new Date)
		@userBench user
		user.activeValidate()
		do @$formBench.find('[data-toggle="checkbox"]').checkbox
		@benchType 'create'

	update: (user) ->
		user.help.fetch().done (data) =>
			user = @help.new data
			@userBench user
			user.activeValidate()
			do @$formBench.find('[data-toggle="checkbox"]').checkbox
			@benchType 'update'
		.fail (data) =>
			do app.Shared.Common.alertError

	submit: (user) ->
		switch @benchType()
			when 'create'
				successMessage = '用户已添加'
			when 'update'
				successMessage = '修改成功'
		@_userSave(user, successMessage, @$formBench)

	submitPoint: (user) ->
		@_userSave(user, '积分保存成功', @$pointBench)

	_userSave: (user, successMessage, $form) ->
		if !user.isValid()
			user.errors.showAllMessages(true)
			return
		user.help.saveChanges().done((data, status) =>
			app.Shared.Common.alert successMessage, 'success'
			@pagination.reload()
			$form.modal('hide')
		).fail (data, status) =>
			app.Shared.Common.alert(data.responseJSON.Message, 'error')
			@help.messageRender data, user

	modifyPoint: (user) ->
		user.help.fetch().done (data) =>
			user = @help.new data
			user.activeValidate()
			@pointBench user
		.fail (data) =>
			do app.Common.alertError

	changeSource: (source) ->
		@sourceType(source.type)
		app.Model.User.prototype.url = source.url
		app.Model.User.prototype.objectStore = source.objectStore
		@pagination.changePage
			index: 1

	resetPwd: (user) ->
		app.Common.confirm.message('确认重置密码?').done ->
			user.resetPwd().done (data, status) =>
				app.Shared.Common.alert '密码已重置', 'success' if status == 'success'

	delete: (user) ->
		app.Common.confirm.message('确认删除?').done ->
			user.help.delete().done (data, status) =>
				app.Shared.Common.alert '删除成功', 'success' if status == 'success'
				@pagination.reload()
