app.Model = app.Model or {}

app.Model.User = class
	constructor: (data) ->
		@help = new app.Shared.ModelHelp @
		@help.init(data)

		@formatTime = ko.computed => moment(@CreateTime()).toDate().toLocaleString()
		@formatIsActive = ko.computed => if @IsActive() then '是' else '否'

		@errors = ko.validation.group(@)

	key: 'PKID'
	url: '/api/CldUsrs'

	persist: [
		'PKID'
		'CName'
		'Name'
		'Email'
		'Point'
		'IsActive'
		'UserType'
		'AuthType'
		'CreateTime'
	]

	activeValidate: ->
		@Name.extend
			required:
				message: '请输入用户名'
		@Email.extend
			email:
				message: '请输入正确的邮箱'
		@Point.extend
			digit:
				message: '请输入数字'

	resetPwd: ->
		$.ajax(@help.requestUrl() + '/invoke/ResetPassword?isInvoke=true', type: 'Patch')