﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UserCenter.Controllers
{
    public class SubSystemController : Controller
    {
        //
        // GET: /BackGround/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /BackGround/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /BackGround/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /BackGround/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /BackGround/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /BackGround/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /BackGround/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /BackGround/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
