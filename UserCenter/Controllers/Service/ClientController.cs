﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using UserCenter.Models;


namespace UserCenter.Controllers
{
    public class ClientController : ApiController
    {
        // POST services/Client/Login
        [HttpPost]
        public HttpResponseMessage Login(ClientLogin model)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            var client = model.ToDomain();
            if (client.Authorize())
            {
                return Request.CreateResponse(HttpStatusCode.OK, new ClientResponse(client));
            }
            else
            {
                ModelState.AddModelError("Password", "密码不正确。");
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // POST: services/Client/Register
        [HttpPost]
        public HttpResponseMessage Register(ClientRegister model)
        {
            if (ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.Created, new ClientResponse(model.ToDomain().CreateAccount()));
            }
            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        // POST: services/Client/VerifyUserName
        [HttpPost]
        public HttpResponseMessage VerifyUserName(ClientVerifyUserName model)
        {
            if (ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new {
                    IsVerified = true
                });
            }
            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        // POST: services/Client/ResetPassword
        [HttpPost]
        public HttpResponseMessage ResetPassword(ClientResetPassword model)
        {
            if (ModelState.IsValid)
            {
                var newPassword = Membership.GeneratePassword(6, 0);
                if (model.ToDomain().ResetPassword(newPassword))
                {
                    string subject = "云平台";
                    string body = "你的密码已重置为：" + newPassword + "，请尽快登录网站修改密码。";
                    MailMessage message = new MailMessage();
                    message.Subject = subject;
                    message.Body = body;
                    message.To.Add(model.Email);

                    SmtpClient smtp = new SmtpClient();
                    smtp.Send(message);

                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }
    }
}
