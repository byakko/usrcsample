﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserCenter.Models;

namespace UserCenter.Controllers.Api
{
    public class UserPointsController : ApiController
    {
        // GET api/UserPoints/5
        public UserPointDto GetUserPoint(Guid id)
        {
            dynamic user = UserPointDomain.FindEntity(id);
            if (user == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return new UserPointDto(new UserPointDomain(user, true));
        }

        // PATCH api/UserPoints/5/invoke/ExchangePoint?isInvoke=true
        [HttpPatch]
        public HttpResponseMessage ExchangePoint(Guid id, bool isInvoke, ExchangePointDto data)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            dynamic user = UserPointDomain.FindEntity(id);
            if (user == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            var userPoint = new UserPointDomain(user, true);
            if (!userPoint.ExchangePoint(data))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
