﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserCenter.Models;

namespace UserCenter.Controllers.Api
{
    public class UserSystemsController : ApiController
    {
        // GET api/UserSystems/5
        public UserSystemDto GetUserProfile(Guid id)
        {
            dynamic userSys = UserSystemDomain.FindEntity(id);
            if (userSys == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return new UserSystemDto(new UserSystemDomain(userSys));
        }
    }
}
