﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using UserCenter.Models;

namespace UserCenter.Controllers
{
    public class BHYFUsrsController : ApiController
    {
        private BHYF_YWTEntities db = new BHYF_YWTEntities();

        // GET api/BHYFUsrs
        public object GetBHYFUsrs(int page)
        {
            var chain = db.bhyf_person.AsQueryable();
            var queryString = Request.RequestUri.ParseQueryString();

            if (!String.IsNullOrEmpty(queryString["Name"]))
            {
                var name = queryString["Name"];
                chain = chain.Where(u => u.name.Contains(name));
            }

            page = page < 1 ? 1 : page;
            int skipNum = 10 * (page - 1);
            return new
            {
                TotalLength = chain.Count(),
                Root = chain.OrderBy(u => u.updatetime).Skip(skipNum).Take(10).AsEnumerable().Select(u => new UserPointDto(new UserPointDomain(u))).AsEnumerable()
            };
        }

        // GET api/BHYFUsrs/5
        public UserPointDto GetBHYFUsr(Guid id)
        {
           bhyf_person bhyfUsr = db.bhyf_person.Where(u => u.memberid == id).FirstOrDefault();
           if (bhyfUsr == null)
           {
               throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
           }

           return new UserPointDto(new UserPointDomain(bhyfUsr));
        }

        // PUT api/BHYFUsrs/5
        public HttpResponseMessage PutBHYFUsr(Guid id, UserPointDto userWithPoint)
        {
           if (!ModelState.IsValid)
           {
               return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
           }

           if (id != userWithPoint.PKID)
           {
               return Request.CreateResponse(HttpStatusCode.BadRequest);
           }

           try
           {
               userWithPoint.ToDomain().SavePoint();
           }
           catch (DbUpdateConcurrencyException ex)
           {
               return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
           }

           return Request.CreateResponse(HttpStatusCode.OK);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}