﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using UserCenter.Models;

namespace UserCenter.Controllers
{
    public class CldUsrsController : ApiController
    {
        private CloudDBEntities db = new CloudDBEntities();

        // GET api/CldUsrs
        public object GetCldUsrs(int page = 1, int limit = 10)
        {
            var chain = db.Cld_User_Main.AsQueryable();
            var queryString = Request.RequestUri.ParseQueryString();

            if (!String.IsNullOrEmpty(queryString["Name"]))
            {
                var name = queryString["Name"];
                chain = chain.Where(u => u.name.Contains(name));
            }

            page = page < 1 ? 1 : page;
            int skipNum = limit * (page - 1);
            return new
            {
                TotalLength = chain.Count(),
                Root = chain.OrderBy(u => u.createtime).Skip(skipNum).Take(limit).AsEnumerable().Select(u => new UserPointDto(new UserPointDomain(u))).AsEnumerable()
            };
        }

        // GET api/CldUsrs/5
        public UserPointDto GetCldUsr(Guid id)
        {
           Cld_User_Main cldUser = db.Cld_User_Main.Find(id);
           if (cldUser == null)
           {
               throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
           }

           return new UserPointDto(new UserPointDomain(cldUser));
        }

        // PUT api/CldUsrs/5
        public HttpResponseMessage PutCldUsr(Guid id, UserPointDto userWithPoint)
        {
           if (!ModelState.IsValid)
           {
               return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
           }

           if (id != userWithPoint.PKID)
           {
               return Request.CreateResponse(HttpStatusCode.BadRequest);
           }

           try
           {
               var user = userWithPoint.ToDomain();
               user.Save();
               user.SavePoint();
           }
           catch (DbUpdateConcurrencyException ex)
           {
               return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
           }

           return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/CldUsrs
        public HttpResponseMessage PostCldUsr(UserPointDto userWithPoint)
        {
           if (ModelState.IsValid)
           {
               userWithPoint.ToDomain().Save();

               // TODO: Response don't work correctly.
               HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, userWithPoint);
               response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = userWithPoint.PKID }));
               return response;
           }
           else
           {
               return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
           }
        }

        // Patch api/CldUsrs/5/invoke/ResetPassword
        [HttpPatch]
        public HttpResponseMessage ResetPassword(Guid id, bool isInvoke)
        {
            var entity = ClientDomain.FindEntity(id);
            if (entity == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            var client = new ClientDomain(entity);
            client.ResetPassword("111111");
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}