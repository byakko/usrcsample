﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserCenter.Models;

namespace UserCenter.Controllers.Api
{
    public class UserProfilesController : ApiController
    {
        // GET api/UserProfiles/5
        public UserProfileDto GetUserProfile(Guid id)
        {
           var profile = UserProfileDomain.Find(id);
           if (profile == null)
           {
               throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
           }

           return new UserProfileDto(profile);
        }

        // PUT api/UserProfiles/5
        public HttpResponseMessage PutUserProfile(Guid id, UserProfileDto profile)
        {
           if (!ModelState.IsValid)
           {
               return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
           }

           if (id != profile.PKID)
           {
               return Request.CreateResponse(HttpStatusCode.BadRequest);
           }

           try
           {
                if (!profile.ToDomain().Save())
                {
	               return Request.CreateResponse(HttpStatusCode.NotFound);
                }
           }
           catch (DbUpdateConcurrencyException ex)
           {
               return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
           }

           return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
