﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using UserCenter.Models;

namespace UserCenter.Controllers
{
    public class SubSystemsController : ApiController
    {
        private UserCenterEntities db = new UserCenterEntities();

        // GET api/SubSystems
        public object GetSubSystems(int page = 1)
        {
            var subSystems = db.Usr_SubSys_Main;
            page = page < 1 ? 1 : page;
            int skipNum = 10 * (page - 1);
            return new
            {
                TotalLength = subSystems.Count(),
                Root = subSystems.OrderBy(s => s.CreateDate).Skip(skipNum).Take(10).AsEnumerable().Select(s => new SubSystemDto(s))
            };
        }

        // GET api/SubSystems/5
        public SubSystemDto GetSubSystem(Guid id)
        {
            Usr_SubSys_Main subSystem = db.Usr_SubSys_Main.Find(id);
            if (subSystem == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return new SubSystemDto(subSystem);
        }

        // PUT api/SubSystems/5
        public HttpResponseMessage PutSubSystem(Guid id, SubSystemDto dto)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != dto.PKID)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(dto.ToEntity()).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/SubSystems
        public HttpResponseMessage PostSubSystem(SubSystemDto dto)
        {
            if (ModelState.IsValid)
            {
                db.Usr_SubSys_Main.Add(dto.ToEntity());
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, dto);
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/SubSystems/5
        public HttpResponseMessage DeleteSubSystem(Guid id)
        {
            Usr_SubSys_Main subSystem = db.Usr_SubSys_Main.Find(id);
            if (subSystem == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            var errorMessage = Usr_SubSys_MainPo.Deletable(subSystem);

            if (errorMessage != null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errorMessage);
            }

            db.Usr_SubSys_Main.Remove(subSystem);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, subSystem);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}