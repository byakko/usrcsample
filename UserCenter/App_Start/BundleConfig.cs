﻿using System.Web;
using System.Web.Optimization;

namespace UserCenter
{
	public class BundleConfig
	{
		// 有关 Bundling 的详细信息，请访问 http://go.microsoft.com/fwlink/?LinkId=254725
		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
						"~/Scripts/jquery-{version}.js"));

			bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
						"~/Scripts/jquery-ui-{version}.js"));

			bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
						"~/Scripts/jquery.unobtrusive*",
						"~/Scripts/jquery.validate*"));

			bundles.Add(new ScriptBundle("~/bundles/indexeddb").Include(
						"~/Scripts/lib/jquery.indexeddb.js"));

			bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
						"~/Scripts/bootstrap.js"));

			bundles.Add(new ScriptBundle("~/bundles/flatui-plugin").Include(
						"~/Scripts/lib/flatui-checkbox.js",
		                "~/Scripts/lib/flatui-radio.js"));

            bundles.Add(new ScriptBundle("~/bundles/account").Include(
                        "~/Scripts/app/account/ajaxlogin.js"));

			bundles.Add(new ScriptBundle("~/bundles/lib").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/jquery-ui-{version}.js",
                "~/Scripts/lib/jquery.indexeddb.js",
                "~/Scripts/knockout-{version}.js",
                "~/Scripts/knockout.validation.js",
                "~/Scripts/underscore.js",
                "~/Scripts/bootstrap.js",
                "~/Scripts/moment.js",
                "~/Scripts/lib/bootstrap-select.js",
                "~/Scripts/lib/bootstrap-switch.js",
                "~/Scripts/lib/flatui-checkbox.js",
                "~/Scripts/lib/flatui-radio.js",
                "~/Scripts/lib/messenger.js"));

			bundles.Add(new ScriptBundle("~/bundles/shared").Include(
                "~/Scripts/app/shared/setting.js",
                "~/Scripts/app/shared/adapter.js",
                "~/Scripts/app/shared/confirm.js",
                "~/Scripts/app/shared/filter.js",
                "~/Scripts/app/shared/pagination.js",
                "~/Scripts/app/shared/modelhelp.js",
                "~/Scripts/app/shared/collectionhelp.js",
                "~/Scripts/app/shared/common.js"));

			bundles.Add(new ScriptBundle("~/bundles/home/index").Include(
                "~/Scripts/app/home/index/common.js",
                "~/Scripts/app/home/index/models/user.js",
                "~/Scripts/app/home/index/models/usercollection.js",
                "~/Scripts/app/home/index/viewmodels/index.js",
                "~/Scripts/app/home/index/main.js"));

			bundles.Add(new ScriptBundle("~/bundles/subsystem/index").Include(
                "~/Scripts/app/subsystem/index/common.js",
                "~/Scripts/app/subsystem/index/models/subsystem.js",
                "~/Scripts/app/subsystem/index/models/subsystemcollection.js",
                "~/Scripts/app/subsystem/index/viewmodels/index.js",
                "~/Scripts/app/subsystem/index/main.js"));

			// 使用 Modernizr 的开发版本进行开发和了解信息。然后，当你做好
			// 生产准备时，请使用 http://modernizr.com 上的生成工具来仅选择所需的测试。
			bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
						"~/Scripts/modernizr-*"));

			bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

			bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
						"~/Content/themes/base/jquery.ui.core.css",
						"~/Content/themes/base/jquery.ui.resizable.css",
						"~/Content/themes/base/jquery.ui.selectable.css",
						"~/Content/themes/base/jquery.ui.accordion.css",
						"~/Content/themes/base/jquery.ui.autocomplete.css",
						"~/Content/themes/base/jquery.ui.button.css",
						"~/Content/themes/base/jquery.ui.dialog.css",
						"~/Content/themes/base/jquery.ui.slider.css",
						"~/Content/themes/base/jquery.ui.tabs.css",
						"~/Content/themes/base/jquery.ui.datepicker.css",
						"~/Content/themes/base/jquery.ui.progressbar.css",
						"~/Content/themes/base/jquery.ui.theme.css"));

			bundles.Add(new StyleBundle("~/Content/themes/all").Include(
                "~/Content/bootstrap/bootstrap.css",
                "~/Content/themes/flat-ui/css/flat-ui.css",
                "~/Content/themes/messenger/messenger.css",
                "~/Content/themes/messenger/messenger-theme-air.css"));

            bundles.Add(new StyleBundle("~/Content/bootstrap/all").Include(
                "~/Content/bootstrap/bootstrap.css"));

			bundles.Add(new StyleBundle("~/Content/themes/flat-ui/css/all").Include(
                "~/Content/themes/flat-ui/css/flat-ui.css"));

			bundles.Add(new StyleBundle("~/Content/themes/messenger/all").Include(
                "~/Content/themes/messenger/messenger.css"));

            bundles.Add(new StyleBundle("~/Content/themes/messenger/theme/air").Include(
                "~/Content/themes/messenger/messenger-theme-air.css"));

            // Not necessary since these will optimize in publish except config is <system.web><compilation debug="true"/></system.web> and run in debug version.
            //BundleTable.EnableOptimizations = true;
		}
	}
}