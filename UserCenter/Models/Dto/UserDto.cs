﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using System.Linq;
using System.Data;

namespace UserCenter.Models
{
    public class UserDto
    {
        private string name;
        public Guid PKID { get; set; }
        [CustomValidation(typeof(UserDto), "NameUnique")]
        public string Name
        {
            get { return name; }
            set { name = value.Trim(); }
        }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> UserType { get; set; }
        public System.DateTime CreateTime { get; set; }
        public Nullable<int> AuthType { get; set; }
        public string CName { get; set; }
        public int? Source { get; set; }

        public UserDto() { }

        public UserDto(UserDomain item)
        {
            PKID = item.PKID;
            Name = item.Name;
            Email = item.Email;
            IsActive = item.IsActive == 1 ? true : false;
            UserType = item.UserType;
            CreateTime = item.CreateTime;
            AuthType = item.AuthType;
            CName = item.CName;
            Source = item.Source;
        }

        public void FillDomain(UserDomain item)
        {
            item.PKID = PKID;
            item.Name = Name;
            item.Email = Email;
            item.IsActive = IsActive == true ? 1 : 0 ;
            item.UserType = UserType;
            item.CreateTime = CreateTime;
            item.AuthType = AuthType;
            item.CName = CName;
            item.Source = Source;
        }

        public UserDomain ToDomain()
        {
            UserDomain domain;
            if (Guid.Empty != PKID)
            {
                domain = new UserDomain(UserDomain.FindEntity(PKID));
            }
            else
            {
                domain = new UserDomain();
            }
            FillDomain(domain);
            return domain;
        }

        public static ValidationResult NameUnique(string value, ValidationContext validationContext)
        {
            dynamic user = validationContext.ObjectInstance;
            // Will not effect to PATCH method without Name
            if (user.PKID != null && user.Name == null)
            {
                return ValidationResult.Success;
            }
            if (user.Name == null)
            {
                return new ValidationResult("请输入用户名。");
            }
            UserDomain persistUser = UserDomain.FindWithName(user.Name);
            if (persistUser != null && (user.PKID == null || user.PKID != persistUser.PKID))
            {
                return new ValidationResult("用户名已存在。");
            }
            else
            {
                return ValidationResult.Success;
            }
        }
    }
}
