﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UserCenter.Models
{
    public class SubSystemDto
    {
        public System.Guid PKID { get; set; }
        public string SysName { get; set; }
        public string AppKey { get; set; }
        public string Path { get; set; }
        public decimal Scale { get; set; }
        [CustomValidation(typeof(SubSystemDto), "CodeUnique")]
        public string Code { get; set; }

        public SubSystemDto() { }

    	public SubSystemDto(Usr_SubSys_Main item)
        {
            PKID = item.PKID;
            SysName = item.SysName;
            AppKey = item.AppKey;
            Path = item.Path;
            Scale = item.Scale;
            Code = item.Code;
        }

        public void FillDomain(Usr_SubSys_Main entity)
        {
            entity.SysName = SysName;
            entity.AppKey = AppKey;
            entity.Path = Path;
            entity.Scale = Scale;
            entity.Code = Code;
        }

        public Usr_SubSys_Main ToEntity()
        {
            Usr_SubSys_Main subSystem;
            if (PKID == Guid.Empty)
            {
                subSystem = new Usr_SubSys_MainPo().ToEntity();
            }
            else
            {
                using (UserCenterEntities db = new UserCenterEntities())
                {
                    subSystem = db.Usr_SubSys_Main.Where(s => s.PKID == PKID).FirstOrDefault();
                }
            }
            FillDomain(subSystem);
            return subSystem;
        }

        public static ValidationResult CodeUnique(string value, ValidationContext validationContext)
        {
            Usr_SubSys_Main entity;
            using (UserCenterEntities db = new UserCenterEntities())
            {
                entity = db.Usr_SubSys_Main.Where(s => s.Code == value).FirstOrDefault();
            }
            if (entity != null && entity.PKID != (validationContext.ObjectInstance as SubSystemDto).PKID)
            {
                return new ValidationResult("该编号已存在。");
            }
            else
            {
                return ValidationResult.Success;
            }
        }

    }
}