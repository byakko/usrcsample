﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserCenter.Models
{
    public class UserSystemDto : UserDto
    {
    	public IEnumerable<UserSubSystemDto> SubSyss { get; set; }

    	public UserSystemDto(UserSystemDomain item)
            : base(item)
        {
            if (item.SubSyss != null)
            {
                SubSyss = item.SubSyss.Select(s => new UserSubSystemDto(s));
            }
        }
    }

    public class UserSubSystemDto
    {
        public System.Guid PKID { get; set; }
        public string SysName { get; set; }
        public string Path { get; set; }

        public UserSubSystemDto(Usr_SubSys_Main item)
        {
			PKID = item.PKID;
			SysName = item.SysName;
			Path = item.Path;
        }
    }
}