﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UserCenter.Models
{
    public class UserProfileDto : UserDto
    {
    	public string Sign { get; set; }
		public string EName { get; set; }
		public Nullable<System.DateTime> Birthday { get; set; }
		public string SocialId { get; set; }
		public string Phone { get; set; }
		public string Mobile { get; set; }
		public Nullable<short> Gender { get; set; }
		public string ZipCode { get; set; }
		public string Address { get; set; }
		public string QQ { get; set; }
		public string Msn { get; set; }
		public string Skype { get; set; }

		public UserProfileDto() { }

        public UserProfileDto(UserProfileDomain item)
            : base(item)
        {
			Sign = item.Sign;
			EName = item.EName;
			Birthday = item.Birthday;
			SocialId = item.SocialId;
			Phone = item.Phone;
			Mobile = item.Mobile;
			Gender = item.Gender;
			ZipCode = item.ZipCode;
			Address = item.Address;
			QQ = item.QQ;
			Msn = item.Msn;
			Skype = item.Skype;
        }

        public void FillDomain(UserProfileDomain item)
        {
            FillDomain(item as UserDomain);
            item.Sign = Sign;
            item.EName = EName;
            item.Birthday = Birthday;
            item.SocialId = SocialId;
            item.Phone = Phone;
            item.Mobile = Mobile;
            item.Gender = Gender;
            item.ZipCode = ZipCode;
            item.Address = Address;
            item.QQ = QQ;
            item.Msn = Msn;
            item.Skype = Skype;
        }

        public new UserProfileDomain ToDomain()
        {
            UserProfileDomain domain;
            if (Guid.Empty != PKID)
            {
                domain = new UserProfileDomain(UserProfileDomain.FindEntity(PKID));
            }
            else
            {
                domain = new UserProfileDomain();
            }
            FillDomain(domain);
            return domain;
        }
    }
}