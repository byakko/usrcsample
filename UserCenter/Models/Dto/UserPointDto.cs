using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using System.Linq;

namespace UserCenter.Models
{
    public class UserPointDto : UserDto
    {
        public decimal Point { get; set; }
        public IEnumerable<SubSysPointDto> SubSyss { get; set; }

        public UserPointDto() { }

        public UserPointDto(UserPointDomain item)
            : base(item)
        {
            Point = item.Point;
            if (item.SubSyss != null)
            {
                SubSyss = item.SubSyss.Select(s => new SubSysPointDto(s));
            }
        }

        public void FillDomain(UserPointDomain item)
        {
            FillDomain(item as UserDomain);
            item.Point = Point;
        }

        public new UserPointDomain ToDomain()
        {
            UserPointDomain domain;
            if (Guid.Empty != PKID)
            {
                domain = new UserPointDomain(UserPointDomain.FindEntity(PKID));
            }
            else
            {
                domain = new UserPointDomain();
            }
            FillDomain(domain);
            return domain;
        }
    }

    public class SubSysPointDto
    {
        public System.Guid UserID { get; set; }
        public System.Guid SubSysID { get; set; }
        public Nullable<decimal> Point { get; set; }
        public string SubSysName { get; set; }
        public decimal Scale { get; set; }

        public SubSysPointDto(SubSysPointDomain item)
        {
            UserID = item.UserID;
            SubSysID = item.SubSysID;
            Point = item.Point;
            SubSysName = item.SubSys.SysName;
            Scale = item.SubSys.Scale;
        }
    }

    public class ExchangePointDto
    {
        public Guid PKID { get; set; }
        public Guid SubSysID { get; set; }
        public int ValueType { get; set; }
        public int Value { get; set; }
    }
}
