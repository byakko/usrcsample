﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using System.Linq;

namespace UserCenter.Models
{
    public class ClientResponse
    {
        public Guid PKID { get; set; }
        public string UserName { get; set; }
        public int? Source { get; set; }

        public ClientResponse(ClientDomain item)
        {
            PKID = item.PKID;
            UserName = item.Name;
            if (item.UserInstance is Cld_User_Main)
            {
                Source = item.Source;
            }
            else
            {
                Source = 0;
            }
        }
    }

    public class ClientLogin
    {
        private string userName;
        [Required]
        [CustomValidation(typeof(ClientVerifyUserName), "NameAvailable")]
        public string UserName
        {
            get { return userName; }
            set { userName = value.Trim(); }
        }
        [Required]
        public string Password { get; set; }

        public bool RememberMe { get; set; }

        public ClientDomain ToDomain()
        {
            var entity = ClientDomain.FindEntityWithName(UserName);
            var client = new ClientDomain(entity);
            client.Name = UserName;
            client.Password = Password;
            return client;
        }
    }

    public class ClientRegister
    {
        private string userName;
        [Required]
        [CustomValidation(typeof(ClientRegister), "NameExist")]
        public string UserName
        {
            get { return userName; }
            set { userName = value.Trim(); }
        }
        [Required]
        public string Password { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public static ValidationResult NameExist(string value, ValidationContext validationContext)
        {
            if (UserDomain.FindWithName(value) == null)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult("用户名已存在。");
            }
        }

        public ClientDomain ToDomain()
        {
            var client = new ClientDomain();
            client.Name = UserName;
            client.Password = Password;
            client.Email = Email;
            return client;
        }
    }

    public class ClientVerifyUserName
    {
        private string userName;
        [Required]
        [CustomValidation(typeof(ClientVerifyUserName), "NameAvailable")]
        public string UserName
        {
            get { return userName; }
            set { userName = value.Trim(); }
        }

        public static ValidationResult NameAvailable(string value, ValidationContext validationContext)
        {
            if (ClientDomain.FindWithName(value) == null)
            {
                return new ValidationResult("用户名不存在。");
            }
            else
            {
                return ValidationResult.Success;
            }
        }
    }

    public class ClientResetPassword
    {
        private string userName;
        [Required]
        public string UserName
        {
            get { return userName; }
            set { userName = value.Trim(); }
        }
        [Required]
        [DataType(DataType.EmailAddress)]
        [CustomValidation(typeof(ClientResetPassword), "EmailMatched")]
        public string Email { get; set; }

        public static ValidationResult EmailMatched(string value, ValidationContext validationContext)
        {
            dynamic user = validationContext.ObjectInstance;
            var persistUser = UserDomain.FindWithName(user.UserName);
            if (persistUser == null)
            {
                return new ValidationResult("用户名不存在。");
            }
            else
            {
                if (user.Email == persistUser.Email)
                {
                    return ValidationResult.Success;
                }
                return new ValidationResult("邮件地址不匹配。");
            }
        }

        public ClientDomain ToDomain()
        {
            var entity = ClientDomain.FindEntityWithName(UserName);
            return entity == null ? null : new ClientDomain(entity);
        }
    }
}
