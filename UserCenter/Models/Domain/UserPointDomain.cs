using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using System.Linq;
using System.Data;

namespace UserCenter.Models
{
    public class UserPointDomain : UserDomain
    {
        public decimal Point { get; set; }
        public Usr_Point PointInstance { get; set; }
        public IEnumerable<SubSysPointDomain> SubSyss { get; set; }

        public UserPointDomain() { }

        public UserPointDomain(Cld_User_Main user, bool includeSubSys = false)
            : base(user)
        {
            Init(includeSubSys);
        }

        public UserPointDomain(bhyf_person person, bool includeSubSys = false)
            : base(person)
        {
            Init(includeSubSys);
        }

        public Usr_Point GetPoint()
        {
            using (UserCenterEntities db = new UserCenterEntities())
            {
                var point = db.Usr_Point.FirstOrDefault(p => p.PKID == PKID);
                if (point == null)
                {
                    point = new Usr_Point();
                    point.PKID = PKID;
                    point.Point = 0;
                    db.Usr_Point.Add(point);
                    db.SaveChanges();
                }
                db.Entry(point).State = EntityState.Detached;
                return point;
            }
        }

        public void SavePoint()
        {
            using (UserCenterEntities db = new UserCenterEntities())
            {
                db.Entry(PointInstance).State = EntityState.Modified;
                PointInstance.Point = Point;
                db.SaveChanges();
                db.Entry(PointInstance).State = EntityState.Detached;
            }
        }

        public IEnumerable<SubSysPointDomain> GetSubSysPoints()
        {
            using (UserCenterEntities db = new UserCenterEntities())
            {
                var chain = db.Usr_SubSys_Point.Where(p => p.UserID == PKID);
                if (chain.Count() == 0)
                {
                    // TODO: TestSystem Only
                    var sysID = Guid.Parse("7c939b68-3dfe-404b-9692-4b0f632359f6");
                    var point = new Usr_SubSys_Point
                    {
                        UserID = PKID,
                        SubSysID = sysID,
                        Point = 0
                    };
                    db.Usr_SubSys_Point.Add(point);
                    db.SaveChanges();
                }
                return chain.ToList().Select(p => new SubSysPointDomain(p, true)).AsEnumerable();
            }
        }

        public bool ExchangePoint(ExchangePointDto data)
        {
            var sys = SubSyss.Where(s => s.SubSysID == data.SubSysID).FirstOrDefault();
            if (sys == null)
            {
                return false;
            }

            switch (data.ValueType)
            {
                case 0:
                    if (PointInstance.Point - data.Value >= 0)
                    {
                        var newPoint = PointInstance.Point - data.Value;
                        var newSubPoint = (decimal)sys.Point + data.Value * sys.SubSys.Scale;
                        ExchangePointPersist(newPoint, newSubPoint, sys);
                        return true;
                    }
                    break;
                case 1:
                    if (sys.Point - data.Value >= 0 && data.Value % sys.SubSys.Scale == 0)
                    {
                        var newPoint = PointInstance.Point + data.Value / sys.SubSys.Scale;
                        var newSubPoint = (decimal)sys.Point - data.Value;
                        ExchangePointPersist(newPoint, newSubPoint, sys);
                        return true;
                    }
                    break;
            }

            return false;
        }

        private void Init(bool includeSubSys)
        {
            PointInstance = GetPoint();
            Point = PointInstance.Point;
            if (includeSubSys)
            {
                SubSyss = GetSubSysPoints();
            }
        }

        private void ExchangePointPersist(decimal newPoint, decimal newSubPoint, SubSysPointDomain sys)
        {
            using (UserCenterEntities db = new UserCenterEntities())
            {
                var subPoint = db.Usr_SubSys_Point.Where(p => p.UserID == PKID && p.SubSysID == sys.SubSysID).FirstOrDefault();
                subPoint.Point = newSubPoint;
                db.SaveChanges();
            }
            Point = newPoint;
            SavePoint();
        }
    }
}
