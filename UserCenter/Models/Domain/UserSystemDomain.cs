﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserCenter.Models
{
    public class UserSystemDomain : UserDomain
    {
    	public IEnumerable<Usr_SubSys_Main> SubSyss { get; set; }

    	public UserSystemDomain(Cld_User_Main user)
            : base(user)
        {
            Init();
        }

        public UserSystemDomain(bhyf_person person)
            : base(person)
        {
            Init();
        }

        private void Init()
        {
	        using (UserCenterEntities db = new UserCenterEntities())
	        {
	        	// TODO: It's only test value
	            var sysID = Guid.Parse("7c939b68-3dfe-404b-9692-4b0f632359f6");
	            SubSyss = db.Usr_SubSys_Main.Where(s => s.PKID == sysID).ToList();
	        }
        }
    }
}