﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace UserCenter.Models
{
    public class UserProfileDomain : UserDomain
    {
        public string Sign { get; set; }
        public string EName { get; set; }
        public Nullable<System.DateTime> Birthday { get; set; }
        public string SocialId { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public Nullable<short> Gender { get; set; }
        public string ZipCode { get; set; }
        public string Address { get; set; }
        public string QQ { get; set; }
        public string Msn { get; set; }
        public string Skype { get; set; }

		public UserProfileDomain() { }

		public UserProfileDomain(Cld_User_Main item) : base(item)
        {
            Sign = item.sign;
            EName = item.ename;
            Birthday = item.birthday;
            SocialId = item.socialid;
            Phone = item.phone;
            Mobile = item.mobile;
            Gender = item.gender;
            ZipCode = item.zipcode;
            Address = item.address;
            QQ = item.qq;
            Msn = item.msn;
            Skype = item.skype;
        }

        public UserProfileDomain(bhyf_person item) : base(item)
        {
            Sign = item.sign;
            EName = item.ename;
            Birthday = item.birthday;
            SocialId = item.socialid;
            Phone = item.phone;
            Mobile = item.mobile;
            Gender = item.gender;
            ZipCode = item.zipcode;
            Address = item.address;
            QQ = item.qq;
            Msn = item.msn;
            Skype = item.skype;
        }

        public static new UserProfileDomain Find(Guid id)
        {
            var user = FindEntity(id);
            return user == null ? null : new UserProfileDomain(user);
        }

        public new bool Save()
        {
            if (UserInstance is Cld_User_Main)
            {
            	using (CloudDBEntities db = new CloudDBEntities())
                {
                    if (UserInstance == null)
                    {
                    	return false;
                    }
                    else
                    {
                        db.Entry(UserInstance as Cld_User_Main).State = EntityState.Modified;
                    }

                    UserInstance.email = Email;
    				UserInstance.sign = Sign;
    				UserInstance.ename = EName;
    				UserInstance.birthday = Birthday;
    				UserInstance.socialid = SocialId;
    				UserInstance.phone = Phone;
    				UserInstance.mobile = Mobile;
    				UserInstance.gender = Gender;
    				UserInstance.zipcode = ZipCode;
    				UserInstance.address = Address;
    				UserInstance.qq = QQ;
    				UserInstance.msn = Msn;
    				UserInstance.skype = Skype;
                    UserInstance.cname = CName;

                    db.SaveChanges();
                    db.Entry(UserInstance as Cld_User_Main).State = EntityState.Detached;
                    return true;
                }
            }
            return false;
        }
    }
}