﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using System.Linq;

namespace UserCenter.Models
{
    public class ClientDomain : UserDomain
    {
        public string Password { get; set; }

        public ClientDomain() { }
        public ClientDomain(Cld_User_Main user) : base(user) { }
        public ClientDomain(bhyf_person person) : base(person) { }

        public bool Authorize()
        {
            if (UserInstance is Cld_User_Main)
            {
                if (UserInstance.password == Agent.Common.Encrypt.MembershipEncrypt(Password, UserInstance.Passwordsalt))
                {
                    return true;
                }
            }

            if (UserInstance is bhyf_person)
            {
                using (BHYF_YWTEntities db = new BHYF_YWTEntities())
                {
                    var member = db.aspnet_Membership.Where(m => m.UserId == PKID).FirstOrDefault();
                    if (member.Password == Agent.Common.Encrypt.MembershipEncrypt(Password, member.PasswordSalt))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public ClientDomain CreateAccount()
        {
            using (CloudDBEntities db = new CloudDBEntities())
            {
                var user = (new Cld_User_MainPo()).toEntity();
                user.name = Name;
                user.email = Email;
                user.password = Agent.Common.Encrypt.MembershipEncrypt(Password, user.Passwordsalt);
                user.source = 1;
                db.Cld_User_Main.Add(user);
                db.SaveChanges();
                db.Entry(user).State = System.Data.EntityState.Detached;
                return new ClientDomain(user);
            }
        }

        public bool ResetPassword(string password)
        {
            if (UserInstance is Cld_User_Main)
            {
                var userEntity = UserInstance as Cld_User_Main;
                userEntity.password = Agent.Common.Encrypt.MembershipEncrypt(password, userEntity.Passwordsalt);
                using (CloudDBEntities db = new CloudDBEntities())
                {
                    db.Entry(userEntity).State = System.Data.EntityState.Modified;
                    db.SaveChanges();
                    db.Entry(userEntity).State = System.Data.EntityState.Detached;
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
