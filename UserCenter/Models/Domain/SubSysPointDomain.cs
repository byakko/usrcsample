﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserCenter.Models
{
    public class SubSysPointDomain
    {
        public System.Guid UserID { get; set; }
        public System.Guid SubSysID { get; set; }
        public Nullable<decimal> Point { get; set; }
        public Usr_SubSys_Main SubSys { get; set; }

        public SubSysPointDomain(Usr_SubSys_Point point, bool withSystem = false)
        {
            UserID = point.UserID;
            SubSysID = point.SubSysID;
            Point = point.Point;

            if (withSystem)
            {
                SubSys = GetSystem();
            }
        }

        public Usr_SubSys_Main GetSystem()
        {
            using (UserCenterEntities db = new UserCenterEntities())
            {
                var sys = db.Usr_SubSys_Main.Find(SubSysID);
                db.Entry(sys).State = System.Data.EntityState.Detached;
                return sys;
            }
        }
    }
}