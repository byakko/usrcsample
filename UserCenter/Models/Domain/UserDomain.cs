﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using System.Linq;
using System.Data;
using System.Reflection;

namespace UserCenter.Models
{
    public class UserDomain
    {
        public Guid PKID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int IsActive { get; set; }
        public Nullable<int> UserType { get; set; }
        public System.DateTime CreateTime { get; set; }
        public Nullable<int> AuthType { get; set; }
        public string CName { get; set; }
        public int? Source { get; set; }
        public dynamic UserInstance { get; set; }

        public UserDomain() { }

        public UserDomain(Cld_User_Main item)
        {
            PKID = item.PKID;
            Name = item.name;
            Email = item.email;
            IsActive = item.IsActive;
            UserType = item.Usertype;
            CreateTime = item.createtime;
            AuthType = item.AuthType;
            CName = item.cname;
            Source = item.source;
            UserInstance = item;
        }

        public UserDomain(bhyf_person item)
        {
            PKID = item.memberid;
            Name = item.name;
            Email = item.email;
            IsActive = item.invisible;
            UserType = 0;
            CreateTime = item.updatetime.Value;
            AuthType = 0;
            CName = item.cname;
            Source = 0;
            UserInstance = item;
        }

        public bool Save()
        {
            using (CloudDBEntities db = new CloudDBEntities())
            {
                if (UserInstance == null)
                {
                    UserInstance = (new Cld_User_MainPo()).toEntity();
                    UserInstance.source = 2;
                    db.Cld_User_Main.Add(UserInstance);
                }
                else
                {
                    db.Entry(UserInstance as Cld_User_Main).State = EntityState.Modified;
                }

                UserInstance.name = Name;
                UserInstance.email = Email;
                UserInstance.IsActive = IsActive;
                // TODO: Full property modification.
                // UserInstance.Usertype = UserType;
                // UserInstance.AuthType = AuthType;
                UserInstance.cname = CName;

                db.SaveChanges();
                db.Entry(UserInstance as Cld_User_Main).State = EntityState.Detached;
                return true;
            }
        }

        public static UserDomain FindWithName(string name)
        {
            var entity = FindEntityWithName(name);
            return entity == null ? null : new UserDomain(entity);
        }

        public static UserDomain Find(Guid id)
        {
            var entity = FindEntity(id);
            return entity == null ? null : new UserDomain(entity);
        }

        public static dynamic FindEntity(Guid id)
        {
            using (CloudDBEntities db = new CloudDBEntities())
            {
                var user = db.Cld_User_Main.Find(id);
                if (user != null)
                {
                    db.Entry(user).State = EntityState.Detached;
                    return user;
                }
            }

            using (BHYF_YWTEntities db = new BHYF_YWTEntities())
            {
                var user = db.bhyf_person.Where(p => p.memberid == id).FirstOrDefault();
                if (user != null)
                {
                    db.Entry(user).State = EntityState.Detached;
                    return user;
                }
            }

            return null;
        }

        public static dynamic FindEntityWithName(string name)
        {
            using (CloudDBEntities db = new CloudDBEntities())
            {
                var user = db.Cld_User_Main.Where(u => u.name == name).FirstOrDefault();
                if (user != null)
                {
                    db.Entry(user).State = EntityState.Detached;
                    return user;
                }
            }

            using (BHYF_YWTEntities db = new BHYF_YWTEntities())
            {
                var user = db.bhyf_person.Where(p => p.name == name).FirstOrDefault();
                if (user != null)
                {
                    db.Entry(user).State = EntityState.Detached;
                    return user;
                }
            }

            return null;
        }
    }
}
