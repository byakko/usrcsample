﻿//------------------------------------------------------------------------------
// <auto-generated>
//    此代码是根据模板生成的。
//
//    手动更改此文件可能会导致应用程序中发生异常行为。
//    如果重新生成代码，则将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace UserCenter.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class UserCenterEntities : DbContext
    {
        public UserCenterEntities()
            : base("name=UserCenterEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<Usr_Point> Usr_Point { get; set; }
        public DbSet<Usr_SubSys_Main> Usr_SubSys_Main { get; set; }
        public DbSet<Usr_SubSys_Point> Usr_SubSys_Point { get; set; }
    }
}
