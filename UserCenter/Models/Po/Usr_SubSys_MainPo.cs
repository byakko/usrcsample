﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Http.ModelBinding;

namespace UserCenter.Models
{
    public class Usr_SubSys_MainPo
    {
        public System.Guid PKID { get; set; }
        public string Code { get; set; }
        public string SysName { get; set; }
        public string AppKey { get; set; }
        public string Path { get; set; }
        public decimal Scale { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }

        public Usr_SubSys_MainPo()
        {
            PKID = Guid.NewGuid();
            CreateDate = DateTime.Now;
        }

        public Usr_SubSys_Main ToEntity()
        {
            Usr_SubSys_Main subSystem = new Usr_SubSys_Main
            {
                PKID = PKID,
                Code = Code,
                SysName = SysName,
                AppKey = AppKey,
                Path = Path,
                Scale = Scale,
                CreateDate = CreateDate
            };
            return subSystem;
        }

        public static string Deletable(Usr_SubSys_Main sys)
        {
            using (UserCenterEntities db = new UserCenterEntities())
            {
                var rec = db.Usr_SubSys_Point.Where(p => p.SubSysID == sys.PKID).FirstOrDefault();
                if (rec == null) return null;
                return "存在关联数据, 无法删除.";
            }
        }
    }
}