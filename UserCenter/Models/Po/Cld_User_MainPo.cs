﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UserCenter.Models
{
    public class Cld_User_MainPo
    {
        public Guid PKID { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public int IsActive { get; set; }
        public string password { get; set; }
        public Nullable<int> source { get; set; }
        public Nullable<int> Usertype { get; set; }
        public System.DateTime createtime { get; set; }
        public string Passwordsalt { get; set; }
        public Nullable<int> AuthType { get; set; }
        public string cname { get; set; }

        public Cld_User_MainPo()
        {
            string passwordsalt = Agent.Common.Encrypt.GetPassWordSalt();
            PKID = Guid.NewGuid();
            IsActive = 1;
            Usertype = 5;
            createtime = DateTime.Now;
            AuthType = 0;
            Passwordsalt = passwordsalt;
            password = Agent.Common.Encrypt.MembershipEncrypt("111111", passwordsalt);
        }

        public Cld_User_MainPo(Cld_User_Main entity)
        {
            PKID = entity.PKID;
            name = entity.name;
            email = entity.email;
            IsActive = entity.IsActive;
            password = entity.password;
            source = entity.source;
            Usertype = entity.Usertype;
            createtime = entity.createtime;
            Passwordsalt = entity.Passwordsalt;
            AuthType = entity.AuthType;
            cname = entity.cname;
        }

        public Cld_User_Main toEntity()
        {
            var entity = new Cld_User_Main();

            entity.PKID = PKID;
            entity.name = name;
            entity.email = email;
            entity.IsActive = IsActive;
            entity.password = password;
            entity.source = source;
            entity.Usertype = Usertype;
            entity.createtime = createtime;
            entity.Passwordsalt = Passwordsalt;
            entity.AuthType = AuthType;
            entity.cname = cname;

            return entity;
        }
    }
}
